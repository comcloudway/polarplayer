#![feature(exclusive_range_pattern)]
extern crate midir;
extern crate termion;
use termion::{
    raw::IntoRawMode,
    async_stdin,
    color,
    cursor
};
use std::{
    error::Error,
    io::{
        stdout,
        Write,
        Read,
    },
    thread,
    time::Duration,
    collections::HashMap
};
use midir::{Ignore, MidiInput};
use sorceress::{
    server::{self, Control, Server},
    synthdef::{encoder::encode_synth_defs, Input, SynthDef},
    ugen,
};

const SUPERCOLLIDER_HOST: &str = "127.0.0.1";
const SUPERCOLLIDER_PORT: &str = "57110";

/// describes the possible screens
enum Screen {
    ConfigureServer,
    SelectMidi,
    Player
}
/// describes available modes
enum Mode {
    /// redirect keys to text field
    Insert,
    /// keybindings
    Normal
}
impl Mode {
    pub fn to_text(&self) -> &str {
        match self {
            Mode::Insert => "Insert",
            Mode::Normal => "Normal"
        }
    }
    pub fn to_color(&self) -> &dyn color::Color {
        match self {
            Mode::Insert => &color::Green,
            Mode::Normal => &color::Yellow
        }
    }
}

enum ExitCode {
    Close,
    NoMidiDeviceFound
}
impl ExitCode {
    pub fn to_msg(&self) -> &str {
        match self {
            ExitCode::Close => "Goodbye, sad to see you go",
            ExitCode::NoMidiDeviceFound => "No Midi Device found"
        }
    }
}

fn one_color(cond: bool, color: &dyn color::Color) -> &dyn color::Color {
    if cond {
        return color;
    } else {
        return &color::Reset;
    }
}

fn main() {
    match run() {
        Ok(_) => (),
        Err(err) => println!("Error: {}", err)
    }
}

fn run() -> Result<(), Box<dyn Error>> {
    // define synths
    let space = {
        SynthDef::new("space", |params| {
            let vol = params.named("vol", 0.0);
            let freq = params.named("freq", 0.0);
            let freq_mod = params.named("pitch", 1.0);

            let vibrato = ugen::SinOsc::ar().freq(freq_mod.mul(freq)).mul(0.005);

            ugen::OffsetOut::ar()
                .bus(0)
                .channels(
                    ugen::Pan2::ar()
                        .input(vibrato.mul(vol)),
                )
        })
    };
    let space_encoded_synthdef = encode_synth_defs(vec![space]);
    let vibrato = {
        SynthDef::new("vibrato", |params| {
            let vol = params.named("vol", 0.0);
            let freq = params.named("freq", 0.0);
            let _freq_mod = params.named("pitch", 1.0);

            let vibrato = ugen::SinOsc::ar().freq(freq).mul(0.005);

            ugen::OffsetOut::ar()
                .bus(0)
                .channels(
                    ugen::Pan2::ar()
                        .input(vibrato.mul(vol)),
                )
        })
    };
    let vibrato_encoded_synthdef = encode_synth_defs(vec![vibrato]);
    let piano = {
        SynthDef::new("piano", |params| {
            let vol = params.named("vol", 0.0);
            let freq = params.named("freq", 0.0);
            let _freq_mod = params.named("pitch", 1.0);

            let vibrato = ugen::SinOsc::ar().freq(freq).mul(0.005);

            ugen::OffsetOut::ar()
                .bus(0)
                .channels(
                    ugen::Pan2::ar()
                        .input(vol.mul(vibrato)),
                )
        })
    };
    let piano_encoded_synthdef = encode_synth_defs(vec![piano]);

    // init termion
    let mut stdout = stdout();

    let mut stdout = stdout.into_raw_mode().unwrap();
    let mut stdin = async_stdin().bytes();
    // reset cursor
    write!(
        stdout,
        "{}{}{}",
        termion::clear::All,
        cursor::Goto(1,1),
        cursor::Hide
    ).unwrap();



    // store state
    let mut screen = Screen::ConfigureServer;
    // force update
    let mut update = true;
    // force prepare
    let mut prepare = true;

    // state
    let mut mode = Mode::Normal;
    let mut ui_item = 0;
    let mut ui_items = 0;

    let mut supercollider_host = String::from(SUPERCOLLIDER_HOST);
    let mut supercollider_port = String::from(SUPERCOLLIDER_PORT);

    let mut midi_selection_index = 0;
    let mut midi_ports = vec![];
    let mut midi_ports_name = vec![];

    let instruments = vec!["space", "vibrato", "piano"];
    let mut instrument = 0;
    let mut instrument_name = instruments[instrument];

    let mut server = None;

    let mut conn:HashMap<&str, midir::MidiInputConnection<()>> = HashMap::new();


    let code = loop {

        // TODO prepare data
        if prepare {
            // general
            mode = Mode::Normal;
            ui_item = 0;

            // screen specific
            match screen {
                Screen::ConfigureServer => {
                    supercollider_host = String::from(SUPERCOLLIDER_HOST);
                    supercollider_port = String::from(SUPERCOLLIDER_PORT);
                    ui_items=2;
                },
                Screen::SelectMidi => {

                    // initialize midir
                    let mut midi_input = MidiInput::new("polarplayer")?;
                    midi_input.ignore(Ignore::None);
                    midi_selection_index = 0;
                    midi_ports = midi_input.ports().to_vec();
                    ui_items = midi_ports.len();
                    midi_ports_name=vec![];
                    for (_i,p) in midi_ports.iter().enumerate() {
                        midi_ports_name.push(midi_input.port_name(p).unwrap().to_string());
                    }
                    if ui_items == 0 {
                        break ExitCode::NoMidiDeviceFound;
                    }
                },
                Screen::Player => {

                    ui_items=1;

                    // disconnect old midi session
                    if let Some(_) = &conn.get("m") {
                        if let Some(d) = conn.remove("m") {
                            d.close();
                        }
                    }
                    // TODO connect to server
                    server = Some(Server::connect(supercollider_host.to_string()+":"+&supercollider_port).unwrap());

                    // register synth to server
                    if let Some(server) = &server {
                        server.send_sync(server::SynthDefRecv::new(&space_encoded_synthdef))?;
                        server.send_sync(server::SynthDefRecv::new(&vibrato_encoded_synthdef))?;
                        server.send_sync(server::SynthDefRecv::new(&piano_encoded_synthdef))?;
                    }

                    // initialize midir
                    let mut midi_input = MidiInput::new("polarplayer")?;
                    midi_input.ignore(Ignore::None);


                    // register midi callback
                    conn.insert("m",midi_input.connect(
                        &midi_ports[midi_selection_index],
                        "polarplayer-input",
                        move |_stamp, message, _| {
                            let code = message[0];
                            let channel = code % 16 + 1;
                            if let Some(supercollider) = &server {
                                match code {
                                    128..145 => {
                                        // NOTE OFF
                                        let _note = message[1];
                                        let _vel = message[2];

                                        // turn note off
                                        let command = server::NodeFree::new(channel as i32..channel as i32 + 1);
                                        supercollider.send(command).unwrap();
                                    },
                                    145..159 => {
                                        // NOTE ON
                                        let note = message[1];
                                        let vel = message[2];
                                        // turn note on
                                        let command = server::SynthNew::new(instrument_name, 0)
                                            .controls(vec![
                                                Control::new("freq", midinote_to_hz(note)),
                                                Control::new("vol", midi_to_hz(vel as f32))
                                            ])
                                            .synth_id(channel as i32);

                                        supercollider.send(command).unwrap();
                                    },
                                    160..176 => {
                                        // POLYPHONIC AFTERTOUCH
                                        // TODO handle aftertouch
                                    },
                                    176..192 => {
                                        // CONTROL/MODE CHANGE
                                    },
                                    192..208 => {
                                        // PROGRAM CHANGE
                                    },
                                    208..224 => {
                                        // CHANNEL AFTERTOUCH - pressure
                                        let controls = vec![Control::new("vol", midi_to_hz(message[1] as f32))];

                                        let command = server::NodeSet::new(channel as i32, controls);
                                        supercollider.send(command).unwrap();
                                    },
                                    224..240 => {
                                        // PITCH WHEEL CONTROL
                                        //TODO handle pitch
                                        let m = midinote_to_hz(message[1]) / 127.0 * if message[2] <= 64 {1.0} else {-1.0};
                                        let controls = vec![Control::new("pitch", m)];

                                        let command = server::NodeSet::new(channel as i32, controls);
                                        supercollider.send(command).unwrap();
                                    }
                                    _ => ()
                                }
                            }
                        },
                        ()
                    ).unwrap());


                }
            }

            prepare = false;
            update = true;
        }

        // TODO handle keypress
        if let Some(Ok(c)) = stdin.next() {
            match mode {
                Mode::Insert => match c {
                    // ESCAPE
                    27 => {
                        update = true;
                        mode = Mode::Normal;
                    },
                    // DELETE
                    127 => {
                        if ui_item == 0 {
                            supercollider_host.pop();
                            update = true;
                        } else if ui_item == 1 {
                            supercollider_port.pop();
                            update = true;
                        }
                    },
                    // ENTER
                    13 => (),
                    c => match screen {
                        Screen::ConfigureServer => {
                            if ui_item == 0 {
                                supercollider_host.push(c as char);
                                update=true;
                            } else if ui_item == 1 {
                                supercollider_port.push(c as char);
                                update=true;
                            }
                        },
                        _ => ()
                    }
                },
                Mode::Normal => match c {
                    b'q' | 27 => {
                        write!(
                            stdout,
                            "{}{}{}{}{}{}{}{}{}{}{}{}{}{}",
                            termion::clear::All,
                            cursor::Goto(5, 1),
                            color::Fg(color::LightCyan),
                            "╔═════════════╗",
                            cursor::Goto(5, 2),
                            "║ POLARPLAYER ║: ",
                            color::Fg(color::Red),
                            ExitCode::Close.to_msg(),
                            color::Fg(color::LightCyan),
                            cursor::Goto(5, 3),
                            "╚═════════════╝",
                            color::Fg(color::Reset),
                            cursor::Goto(5, 5),
                            cursor::Show
                        ).unwrap();
                        drop(stdout);

                        if let Some(_) = &conn.get("m") {
                            if let Some(d) = conn.remove("m") {
                                d.close();
                            }
                        }

                        break ExitCode::Close;
                    },
                    b'i' | b'a'=> {
                        update = true;
                        mode = Mode::Insert;
                    },
                    b'k' => {
                        if ui_item>0 {
                            ui_item-=1;
                        } else {
                            ui_item = (ui_items as isize - 1).abs() as usize;
                        }
                        update=true;
                    },
                    b'j' => {
                        if ui_item+1<ui_items {
                            ui_item+=1;
                        } else {
                            ui_item=0;
                        }
                        update=true;
                    },
                    b'r' => match screen {
                        Screen::SelectMidi => {
                            prepare=true;
                        }
                        _ => ()
                    },
                    b'h' => match screen {
                        Screen::Player => match ui_item {
                            0 => {
                                if instrument==0 {
                                    instrument = instruments.len()-1;
                                } else {
                                    instrument-=1;
                                }

                                instrument_name=instruments[instrument];
                                update=true;
                                prepare=true;
                            },
                            _ => ()
                        },
                        _ => ()
                    },
                    b'l' => match screen {
                        Screen::Player => match ui_item {
                            0 => {
                                if instrument+1 >= instruments.len() {
                                    instrument = 0;
                                } else {
                                    instrument+=1;
                                }

                                instrument_name=instruments[instrument];
                                update=true;
                                prepare=true;
                            },
                            _ => ()
                        }
                        _ => ()
                    }
                    // confirm
                    b'c' | 13 => match screen {
                        Screen::ConfigureServer => {
                            screen = Screen::SelectMidi;
                            prepare=true;
                        },
                        Screen::SelectMidi => {
                            screen = Screen::Player;
                            midi_selection_index=ui_item;
                            prepare=true;
                        },
                        _ => ()
                    }
                    _ => ()
                }
            }
        }

        // TODO render screen
        if update {
            update = false;

            write!(
                stdout,
                "{}{}{}{}{}{}{}{}{}{}{}{}",
                termion::clear::All,
                cursor::Goto(5, 1),
                color::Fg(color::LightCyan),
                "╔═════════════╗",
                cursor::Goto(5, 2),
                "║ POLARPLAYER ║",
                color::Fg(mode.to_color()),
                " ▌".to_owned() + &mode.to_text().to_uppercase(),
                color::Fg(color::LightCyan),
                cursor::Goto(5, 3),
                "╚═════════════╝",
                color::Fg(color::Reset),
            ).unwrap();

            match screen {
                Screen::ConfigureServer => {
                    write!(
                        stdout,
                        "{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}",
                        cursor::Goto(5, 5),
                        "┌────────────────────────────────┐",
                        cursor::Goto(5, 6),
                        "│ Configure SuperCollider Server │",
                        cursor::Goto(5, 7),
                        "├────────────────────────────────┘",
                        cursor::Goto(5, 8),
                        color::Fg(one_color(ui_item==0,&color::Red)),
                        "│ HOST: ".to_owned() + &supercollider_host,
                        color::Fg(color::Reset),
                        cursor::Goto(5, 9),
                        "│",
                        cursor::Goto(5, 10),
                        color::Fg(one_color(ui_item==1,&color::Red)),
                        "│ PORT: ".to_owned() + &supercollider_port,
                        color::Fg(color::Reset),
                        cursor::Goto(5, 11),
                        "╵"
                    ).unwrap();
                },
                Screen::SelectMidi => {
                    write!(
                        stdout,
                        "{}{}{}{}{}{}{}",
                        cursor::Goto(5, 5),
                        "┌──────────────────────────┐",
                        cursor::Goto(5, 6),
                        "│ Select Midi Input Device │",
                        cursor::Goto(5, 7),
                        "├──────────────────────────┘",
                        cursor::Goto(5, 8),
                    ).unwrap();
                    for (i, _p) in midi_ports.iter().enumerate() {
                        write!(
                            stdout,
                            "{}{}{}{}{}{}",
                            cursor::Goto(5, 7 as u16 +(2*i as u16+1 as u16)),
                            "│",
                            cursor::Goto(5, 7 as u16 +((2*i as u16+2))),
                            color::Fg(one_color(ui_item==i,&color::Red)),
                            "│ ".to_owned() + &midi_ports_name[i],
                            color::Fg(color::Reset)
                        ).unwrap();
                    }
                    write!(
                        stdout,
                        "{}{}",
                        cursor::Goto(5, 7 as u16 + midi_ports.len() as u16 * 2 + 1),
                        "╵"
                    ).unwrap();
                },
                Screen::Player => {
                    write!(
                        stdout,
                        "{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}",
                        cursor::Goto(5, 5),
                        "┌─────────────┐",
                        cursor::Goto(5, 6),
                        "│ Information │",
                        cursor::Goto(5, 7),
                        "├─────────────┘",
                        cursor::Goto(5, 8),
                        "│ HOST: ".to_owned() + &supercollider_host,
                        cursor::Goto(5, 9),
                        "│",
                        cursor::Goto(5, 10),
                        "│ PORT: ".to_owned() + &supercollider_port,
                        cursor::Goto(5, 11),
                        "│",
                        cursor::Goto(5, 12),
                        "│ MIDI: ".to_owned() + &midi_ports_name[midi_selection_index],
                        cursor::Goto(5, 13),
                        "│",
                        cursor::Goto(5, 14),
                        "├───────────────┐",
                        cursor::Goto(5, 15),
                        "│ Configuration │",
                        cursor::Goto(5, 16),
                        "├───────────────┘",
                        cursor::Goto(5, 17),
                        color::Fg(one_color(ui_item==0,&color::Red)),
                        "│ INSTR: ",
                    ).unwrap();
                    for (i, item) in instruments.iter().enumerate() {
                        write!(
                            stdout,
                            "{}{}{}{}{} ",
                            color::Fg(one_color(instrument==i,&color::Magenta)),
                            if instrument==i {"["} else {" "},
                            item,
                            if instrument==i {"]"} else {" "},
                            color::Fg(color::Reset)
                        ).unwrap();
                    }
                    write!(
                        stdout,
                        "{}{}{}",
                        color::Fg(color::Reset),
                        cursor::Goto(5, 18),
                        "╵",
                    ).unwrap();
                }
            }
        }

        thread::sleep(Duration::from_millis(50));
        stdout.flush().unwrap();
    };

    Ok(())
}


/// Convert a midi note number into a frequency in hertz.
/// @source: https://docs.rs/sorceress/latest/sorceress/
fn midi_to_hz(note: f32) -> f32 {
    let exp = (note - 69.0) / 12.0;
    440.0 * 2f32.powf(exp)
}


fn midinote_to_hz(note:u8) -> f32 {
    let note = 440.0 * 2.0_f32.powf((note as i8 - 69) as f32/12.0);
    return note;
}
